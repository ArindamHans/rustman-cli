# Rustman-TUI

A minimal API testing tool in Rust, similar to Postman.

![Screenshot](screenshot.webp)

## Features
- [x] Use OpenAPI specs for API collections.
- [x] Send HTTP requests (GET, POST, PUT, PATCH, DELETE).
- [x] Configure query parameters.
- [x] Set JSON body in requests.
- [x] Set custom headers in requests.
- [ ] Cookie support (Planned).
- [ ] Authorizatoin support (Planned).
- [ ] Enhanced UI with more functionalities (Planned).
- [ ] Edit API collections within the application (Planned).
- [ ] Improved error handling and feedback (Planned).

## Installation
To run this project, you need to have Rust installed.

1. Clone the repository:
    ```bash
    git clone https://gitlab.com/ArindamHans/rustman-cli.git
    ```
2. Navigate to the project directory:
    ```bash
    cd rustman-cli
    ```
3. Build the project:
    ```bash
    cargo build
    ```
4. Run the project:
    ```bash
    cargo run -- <path_to_your_openapi_spec>
    ```

## Usage
To start the application, run:
```bash
cargo run -- <path_to_your_openapi_spec>
```
