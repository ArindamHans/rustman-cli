use crate::app::{App, ResponseData};
use regex::Regex;
use reqwest::Client;
use serde_json;
use std::error::Error;

fn set_query_params<'a>(app: &'a App, request: reqwest::RequestBuilder) -> reqwest::RequestBuilder {
    let param_regex = Regex::new(r#"(\w+)\s*=\s*(\w+)"#).unwrap();

    let mut param_map = vec![];

    // Parse each string using regex and add the key-value pairs to the Query
    for line in app.req_params_textarea.lines() {
        if let Some(cap) = param_regex.captures(line) {
            if let (Some(key), Some(value)) = (cap.get(1), cap.get(2)) {
                param_map.push((key.as_str(), value.as_str()));
            }
        }
    }

    request.query(&param_map)
}

/* HTTP REQUEST HANDLER */
pub async fn send_request<'a>(app: &mut App<'a>) -> Result<(), Box<dyn Error>> {
    // CLIENT
    let client = Client::builder().build()?;
    let mut request = client.request(
        app.request.http_method.to_reqwest_method(),
        &app.url_textarea.lines()[0],
    );

    // Set params -------------------------------------
    request = set_query_params(app, request);

    // Set request header -----------------------------
    // Define the regex pattern to match key-value pairs
    let header_regex = Regex::new(r#""(\w+)":\s*"(.*?)""#).unwrap();

    // Parse each string using regex and add the key-value pairs to the HeaderMap
    for line in app.req_headers_textarea.lines() {
        if let Some(cap) = header_regex.captures(line) {
            if let (Some(key), Some(value)) = (cap.get(1), cap.get(2)) {
                request = request.header(key.as_str(), value.as_str());
            }
        }
    }

    // Set request body -------------------------------
    // Concatenate the strings into a single JSON string
    app.request.body = app.req_body_textarea.lines().join("\n");

    // Check if the body string is empty
    if !app.request.body.trim().is_empty() {
        // Parse the body string as JSON
        let json_body_result: Result<serde_json::Value, serde_json::Error> =
            serde_json::from_str(&app.request.body);
        match json_body_result {
            Ok(json_body) => {
                request = request.json(&json_body);
            }
            Err(err) => {
                println!("Error parsing JSON body: {}", err);
                return Err(Box::new(err));
            }
        }
    }

    // Send request -----------------------------------
    let response = request.send().await?;

    let status = response.status();
    let response_body = response.text().await?;
    let body = match serde_json::from_str::<serde_json::Value>(&response_body) {
        Ok(json) => serde_json::to_string_pretty(&json).unwrap_or(response_body),
        Err(_) => response_body,
    };
    let body_line_count: u16 = body.lines().count().try_into().unwrap();

    app.response = Some(ResponseData {
        status,
        body,
        body_line_count,
    });

    Ok(())
}
