use crate::enums::{HTTPMethod, Mode};
use crate::openapi::{convert_hashmap_to_treeitems, populate_api_details};
use ratatui::widgets::*;
use reqwest::StatusCode;
use serde::{Deserialize, Serialize};
use serde_json;
use std::collections::HashMap;
use std::env;
use tui_textarea::TextArea;
use tui_tree_widget::{TreeItem, TreeState};

#[derive(Debug)]
pub struct App<'a> {
    pub request: RequestData,
    pub response: Option<ResponseData>,
    pub selected: usize,
    pub previous_selected: usize,
    pub mode: Mode,
    pub method_popup_open: bool,
    pub focused_method_index: usize,
    pub active_req_tab_index: usize,
    pub method_list_state: ListState,
    pub url_textarea: TextArea<'a>,
    pub req_params_textarea: TextArea<'a>,
    pub req_headers_textarea: TextArea<'a>,
    pub req_body_textarea: TextArea<'a>,
    pub res_scroll: u16,
    pub api_details: Api,
    pub api_tree_items: Vec<TreeItem<'a, String>>,
    pub api_tree_state: TreeState<String>,
}

impl<'a> App<'a> {
    pub fn new() -> Result<Self, String> {
        // Get command-line arguments
        let args: Vec<String> = env::args().collect();

        // Check if there are enough arguments
        if args.len() < 2 {
            return Err("Please provide swagger config file path.".to_string());
        }

        let swagger_path = &args[1];

        let api_details = populate_api_details(swagger_path).unwrap();
        let api_tree_items = convert_hashmap_to_treeitems(api_details.api_map.clone());

        let mut textarea = TextArea::default();
        textarea.set_block(Block::default().borders(Borders::ALL));

        Ok(App {
            request: RequestData::new(),
            response: None,
            selected: 1,
            previous_selected: 1,
            mode: Mode::Normal,
            method_popup_open: false,
            focused_method_index: 0,
            active_req_tab_index: 1,
            method_list_state: ListState::default(),
            url_textarea: textarea.clone(),
            req_params_textarea: textarea.clone(),
            req_headers_textarea: textarea.clone(),
            req_body_textarea: textarea.clone(),
            res_scroll: 0,
            api_details: api_details,
            api_tree_items: api_tree_items,
            api_tree_state: TreeState::default(),
        })
    }

    pub fn set_api_tree(&mut self, selected_tree_item: Vec<String>) {
        match selected_tree_item.as_slice() {
            [tag, path, method] => {
                let api_tree = self
                    .api_details
                    .api_map
                    .get(tag)
                    .unwrap()
                    .get(path)
                    .unwrap()
                    .get(method)
                    .unwrap();

                self.url_textarea = TextArea::new(vec![self.api_details.base_url.clone() + path]);
                self.request.http_method = HTTPMethod::from_str(&api_tree.http_method).unwrap();
            }
            _ => {}
        }
    }
}

// Struct to hold reqwest request data
#[derive(Debug, Clone)]
pub struct RequestData {
    pub http_method: HTTPMethod,
    pub body: String,
    pub headers: Vec<(String, String)>,
}

impl RequestData {
    pub fn new() -> Self {
        RequestData {
            http_method: HTTPMethod::GET,
            body: String::new(),
            headers: Vec::new(),
        }
    }
}

#[derive(Debug)]
pub struct Api {
    pub base_url: String,
    pub api_map: HashMap<String, HashMap<String, HashMap<String, ApiEndpoint>>>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ApiEndpoint {
    pub http_method: String,
    pub summary: Option<String>,
    pub parameters: Option<serde_json::Value>,
    pub responses: Option<serde_json::Value>,
}

// Struct to hold reqwest response data
#[derive(Debug)]
pub struct ResponseData {
    pub status: StatusCode,
    pub body: String,
    pub body_line_count: u16,
}

// Request Setting Tab Titles
pub static REQ_TAB_TITLES: [&'static str; 3] = ["[1] Params", "[2] Headers", "[3] Body"];
