use crate::app::App;
use crate::enums::{HTTPMethod, Mode};
use crate::http::send_request;

use crossterm::event::{poll, read, Event, KeyCode, KeyEvent, KeyModifiers};
use std::io;
use tokio::time::Duration;
use tui_textarea::Input;

// TODO: Have to change 'app.selected' to string instead of number
// as it would make understanding which block is being selected more clear
/*
'app.selected' number representation for blocks -
0 => HTTP Method Dropdown Block
1 => URL Input Box Block
2 => Params Block
3 => Response Block
4 => Collections Block
*/

// TODO: Mouse handle

pub async fn handle_input<'a>(app: &mut App<'a>) -> Result<(), io::Error> {
    if poll(Duration::from_millis(500))? {
        if let Event::Key(key) = read()? {
            match app.mode {
                Mode::Insert => handle_insert_mode_input(app, key).await?,
                Mode::Normal => handle_normal_mode_input(app, key).await?,
            }
        }
    }
    Ok(())
}

async fn handle_insert_mode_input<'a>(app: &mut App<'a>, key: KeyEvent) -> Result<(), io::Error> {
    let textarea_input = Input::from(key);

    // Handle common cases across all app.selected values
    match key.code {
        KeyCode::Esc => {
            app.mode = Mode::Normal;
        }
        KeyCode::Char('c') if key.modifiers.contains(KeyModifiers::CONTROL) => {
            return Err(io::Error::new(io::ErrorKind::Other, "Quit"));
        }
        _ => {}
    }

    // Handle cases specific to each app.selected value
    match app.selected {
        0 => match key.code {
            KeyCode::Up if app.method_popup_open && app.focused_method_index > 0 => {
                app.focused_method_index -= 1;
            }
            KeyCode::Down
                if app.method_popup_open
                    && app.focused_method_index < HTTPMethod::variants().len() - 1 =>
            {
                app.focused_method_index += 1;
            }
            _ => {}
        },
        1 => match key.code {
            KeyCode::Enter => {
                if let Err(err) = send_request(app).await {
                    println!("Error sending request: {}", err);
                }
            }
            _ => {
                app.url_textarea.input(textarea_input);
            }
        },
        2 => match app.active_req_tab_index {
            1 => {
                app.req_params_textarea.input(textarea_input);
            }
            2 => {
                app.req_headers_textarea.input(textarea_input);
            }
            3 => {
                app.req_body_textarea.input(textarea_input);
            }
            _ => {}
        },
        4 => match key.code {
            KeyCode::Left | KeyCode::Char('h') => {
                app.api_tree_state.key_left();
            }
            KeyCode::Right | KeyCode::Char('l') => {
                app.api_tree_state.key_right();
            }
            KeyCode::Up | KeyCode::Char('k') => {
                app.api_tree_state.key_up(&app.api_tree_items);
            }
            KeyCode::Down | KeyCode::Char('j') => {
                app.api_tree_state.key_down(&app.api_tree_items);
            }
            KeyCode::Enter => {
                let selected_tree_item = app.api_tree_state.selected();
                app.set_api_tree(selected_tree_item);
            }
            _ => {}
        },
        _ => {}
    }

    Ok(())
}

async fn handle_normal_mode_input<'a>(app: &mut App<'a>, key: KeyEvent) -> Result<(), io::Error> {
    // Handle common cases across all app.selected values
    match key.code {
        KeyCode::Char('i') => {
            app.mode = Mode::Insert;
        }
        KeyCode::Char('q') => {
            return Err(io::Error::new(io::ErrorKind::Other, "Quit"));
        }
        _ => {}
    }

    // Handle cases specific to each app.selected value
    match app.selected {
        0 => {
            if app.method_popup_open {
                match key.code {
                    KeyCode::Enter => {
                        app.request.http_method = HTTPMethod::variants()[app.focused_method_index];
                        app.method_popup_open = !app.method_popup_open;
                    }
                    KeyCode::Up | KeyCode::Char('k') if app.focused_method_index > 0 => {
                        app.focused_method_index -= 1;
                    }
                    KeyCode::Down | KeyCode::Char('j')
                        if app.focused_method_index < HTTPMethod::variants().len() - 1 =>
                    {
                        app.focused_method_index += 1;
                    }
                    _ => {}
                }
            } else {
                match key.code {
                    KeyCode::Enter => {
                        app.method_popup_open = true;
                    }
                    KeyCode::Left | KeyCode::Char('h') => {
                        app.previous_selected = 0;
                        app.selected = 4;
                    }
                    KeyCode::Right | KeyCode::Char('l') => {
                        app.selected = 1;
                    }
                    KeyCode::Down | KeyCode::Char('j') => {
                        app.selected = 2;
                    }
                    _ => {}
                }
            }
        }
        1 => match key.code {
            KeyCode::Enter => {
                let _ = send_request(app).await;
            }
            KeyCode::Left | KeyCode::Char('h') => {
                app.selected = 0;
            }
            KeyCode::Right | KeyCode::Char('l') => {
                app.selected = 2;
            }
            KeyCode::Down | KeyCode::Char('j') => {
                app.selected = 2;
            }
            _ => {}
        },
        2 => match key.code {
            KeyCode::Left | KeyCode::Char('h') => {
                app.previous_selected = 2;
                app.selected = 4;
            }
            KeyCode::Up | KeyCode::Char('k') => {
                app.selected = 1;
            }
            KeyCode::Right | KeyCode::Char('l') => {
                app.selected = 3;
            }
            KeyCode::Down | KeyCode::Char('j') => {
                app.selected = 3;
            }
            KeyCode::Char('1') => {
                app.active_req_tab_index = 1;
            }
            KeyCode::Char('2') => {
                app.active_req_tab_index = 2;
            }
            KeyCode::Char('3') => {
                app.active_req_tab_index = 3;
            }
            _ => {}
        },
        3 => match key.code {
            KeyCode::Left | KeyCode::Char('h') => {
                app.previous_selected = 3;
                app.selected = 4;
            }
            KeyCode::Up | KeyCode::Char('k') => {
                if key.modifiers.contains(KeyModifiers::CONTROL) && app.res_scroll > 0 {
                    app.res_scroll -= 1;
                } else {
                    app.selected = 2;
                }
            }
            KeyCode::Down | KeyCode::Char('j')
                if key.modifiers.contains(KeyModifiers::CONTROL)
                    && app.res_scroll < app.response.as_ref().map_or(0, |r| r.body_line_count) =>
            {
                app.res_scroll += 1;
            }
            KeyCode::Char('u')
                if key.modifiers.contains(KeyModifiers::CONTROL) && app.res_scroll >= 10 =>
            {
                app.res_scroll -= 10;
            }
            KeyCode::Char('d')
                if key.modifiers.contains(KeyModifiers::CONTROL)
                    && app.res_scroll
                        <= app.response.as_ref().map_or(0, |r| r.body_line_count) - 10 =>
            {
                app.res_scroll += 10;
            }
            _ => {}
        },
        4 => match key.code {
            KeyCode::Right | KeyCode::Char('l') => {
                app.selected = app.previous_selected;
            }
            _ => {}
        },
        _ => {}
    }
    Ok(())
}
