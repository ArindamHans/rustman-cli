use crate::app::{App, ResponseData, REQ_TAB_TITLES};
use crate::enums::{HTTPMethod, Mode};

use ratatui::{
    backend::Backend,
    layout::{Alignment, Constraint, Direction, Layout, Rect},
    prelude::*,
    style::{Color, Style},
    widgets::*,
    Frame, Terminal,
};
use serde_json;
use tui_textarea::TextArea;
use tui_tree_widget::Tree;

pub fn draw_ui<'a, B: Backend>(
    terminal: &mut Terminal<B>,
    app: &mut App<'a>,
) -> Result<(), std::io::Error> {
    terminal.draw(|f| {
        // Highlight colors for selectable blocks
        let mut block_highlight_colors = vec![Color::White; 5];

        // Update highlight colors
        match app.selected {
            0 => block_highlight_colors[0] = Color::Yellow,
            1 => block_highlight_colors[1] = Color::Yellow,
            2 => block_highlight_colors[2] = Color::Yellow,
            3 => block_highlight_colors[3] = Color::Yellow,
            4 => block_highlight_colors[4] = Color::Yellow,
            _ => {}
        }

        let root_chunks = setup_root_layout(f.size());
        let left_column_chunk = root_chunks[0]; // Access left column chunk
        let right_column_chunk = root_chunks[1..root_chunks.len() - 1].to_vec(); // Access right column chunk
        let footer_chunk = root_chunks.last().expect("Footer chunk is missing"); // Access footer chunk
        let method_url_chunk = setup_method_url_block(right_column_chunk[0]);

        render_req_tabs_block(
            f,
            &app.active_req_tab_index,
            &block_highlight_colors[2],
            right_column_chunk[1],
            &mut app.req_params_textarea,
            &mut app.req_headers_textarea,
            &mut app.req_body_textarea,
        );
        render_response_output(
            f,
            &app.response,
            &app.res_scroll,
            &block_highlight_colors[3],
            right_column_chunk[2],
        );
        render_url_input(
            f,
            &mut app.url_textarea,
            &block_highlight_colors[1],
            method_url_chunk[1],
        );
        render_method_block(
            f,
            app.request.http_method,
            app.method_popup_open,
            &mut app.method_list_state,
            app.focused_method_index,
            &block_highlight_colors[0],
            method_url_chunk[0],
        );
        render_api_collections_block(f, left_column_chunk, app, &block_highlight_colors[4]);
        render_footer(f, &app.mode, *footer_chunk);
    })?;

    Ok(())
}

fn setup_root_layout(size: Rect) -> Vec<Rect> {
    let chunks = Layout::default()
        .direction(Direction::Vertical) // Change direction to vertical
        .constraints(
            [
                Constraint::Percentage(95), // 90% for main content
                Constraint::Percentage(5),  // 10% for footer
            ]
            .as_ref(),
        )
        .split(size);

    let main_content_chunk = chunks[0]; // Chunk for main content
    let footer_chunk = chunks[1]; // Chunk for footer

    // Setup main content layout similar to previous setup
    let main_content_layout = Layout::default()
        .direction(Direction::Horizontal)
        .constraints(
            [
                Constraint::Percentage(20), // Collections menu column
                Constraint::Percentage(80),
            ]
            .as_ref(),
        )
        .split(main_content_chunk);

    let right_column_chunks = Layout::default()
        .direction(Direction::Vertical)
        .constraints(
            [
                Constraint::Length(3),      // For URL input and method dropdown
                Constraint::Percentage(40), // For Request settings
                Constraint::Min(0),         // For response output
            ]
            .as_ref(),
        )
        .split(main_content_layout[1]);

    let left_column = main_content_layout[0];

    let mut layout = Vec::new();
    layout.push(left_column);
    layout.extend(right_column_chunks.to_vec());

    layout.push(footer_chunk); // Add footer chunk to the layout

    layout
}

fn setup_method_url_block(area: Rect) -> Vec<Rect> {
    Layout::default()
        .direction(Direction::Horizontal)
        .constraints(
            [
                Constraint::Percentage(15), // For method dropdown
                Constraint::Percentage(85), // For URL input
            ]
            .as_ref(),
        )
        .split(area)
        .to_vec()
}

// Function to draw the API Collection Tree Menu
// HACK: passing 'app' as the param instead of only the 'api_tree_state'
fn render_api_collections_block(
    f: &mut Frame,
    area: Rect,
    app: &mut App,
    block_highlight_color: &Color,
) {
    let api_collections_block = Block::default()
        .borders(Borders::ALL)
        .border_style(Style::default().fg(*block_highlight_color))
        .padding(Padding::uniform(1))
        .title("Collections")
        .title_alignment(Alignment::Center);

    let tree_widget = Tree::new(app.api_tree_items.clone())
        .expect("all item identifiers are unique")
        .block(api_collections_block)
        .experimental_scrollbar(Some(
            Scrollbar::new(ScrollbarOrientation::VerticalRight)
                .begin_symbol(None)
                .track_symbol(None)
                .end_symbol(None),
        ))
        .highlight_style(Style::default().fg(Color::Yellow));

    f.render_stateful_widget(tree_widget, area, &mut app.api_tree_state);
}

fn render_method_block(
    f: &mut Frame,
    method: HTTPMethod,
    method_popup_open: bool,
    method_list_state: &mut ListState,
    focused_method_index: usize,
    block_highlight_color: &Color,
    area: Rect,
) {
    let remaining_space = area.width as usize - method.to_string().len() - 5;
    let method_block = Paragraph::new(format!(
        " {}{}🞃 ",
        method.to_string(),
        " ".repeat(remaining_space)
    ))
    .style(Style::default().fg(*block_highlight_color))
    .block(Block::default().title("Method").borders(Borders::ALL));

    f.render_widget(method_block, area);

    if method_popup_open {
        let popup_width = area.width;
        let popup_height = area.height + 4;
        let popup_x = area.x;
        let popup_y = area.y;

        let popup_area = Rect::new(popup_x, popup_y, popup_width, popup_height);

        let items: Vec<_> = HTTPMethod::variants()
            .iter()
            .map(|&m| ListItem::new(m.to_string()))
            .collect();
        let method_dropdown = List::new(items)
            .style(Style::default().fg(*block_highlight_color))
            .highlight_style(Style::default().fg(Color::DarkGray).bg(Color::Yellow))
            .block(Block::default().title("Method").borders(Borders::ALL));

        f.render_widget(Clear, popup_area);
        method_list_state.select(Some(focused_method_index));
        f.render_stateful_widget(method_dropdown, popup_area, method_list_state);
        f.set_cursor(popup_area.x + 1, popup_area.y + 1);
    }
}

fn render_url_input(
    f: &mut Frame,
    url_textarea: &mut TextArea,
    block_highlight_color: &Color,
    area: Rect,
) {
    let url_input_block = Block::default()
        .title("URL")
        .borders(Borders::ALL)
        .style(Style::default().fg(*block_highlight_color))
        .padding(Padding::horizontal(1));
    url_textarea.set_block(url_input_block);
    let url_input = url_textarea.widget();

    f.render_widget(url_input, area);
}

fn render_req_tabs_block(
    f: &mut Frame,
    active_req_tab_index: &usize,
    block_highlight_color: &Color,
    area: Rect,
    req_params_textarea: &mut TextArea,
    req_headers_textarea: &mut TextArea,
    req_body_textarea: &mut TextArea,
) {
    let req_tab_area = area;

    match active_req_tab_index {
        1 => {
            // Params Block
            let req_params_block = Block::default()
                .borders(Borders::ALL)
                .style(Style::default().fg(*block_highlight_color))
                .padding(Padding::uniform(1));
            req_params_textarea.set_block(req_params_block);
            let req_params = req_params_textarea.widget();

            f.render_widget(req_params, req_tab_area);
        }
        2 => {
            // Header Block
            let req_headers_block = Block::default()
                .borders(Borders::ALL)
                .style(Style::default().fg(*block_highlight_color))
                .padding(Padding::uniform(1));
            req_headers_textarea.set_block(req_headers_block);
            let req_headers = req_headers_textarea.widget();

            f.render_widget(req_headers, req_tab_area);
        }
        3 => {
            // Request Body Block
            let req_body_block = Block::default()
                .borders(Borders::ALL)
                .style(Style::default().fg(*block_highlight_color))
                .padding(Padding::uniform(1));
            req_body_textarea.set_block(req_body_block);
            let req_body = req_body_textarea.widget();

            f.render_widget(req_body, req_tab_area);
        }
        _ => {}
    }

    let req_tabs_block = Tabs::new(REQ_TAB_TITLES.iter().cloned())
        .block(Block::default())
        .select(*active_req_tab_index - 1)
        .padding_left(" ")
        .divider("━");
    f.render_widget(req_tabs_block, area);
}

fn render_response_output(
    f: &mut Frame,
    response: &Option<ResponseData>,
    res_scroll: &u16,
    block_highlight_color: &Color,
    area: Rect,
) {
    let response_block = Block::default()
        .title("Response")
        .borders(Borders::ALL)
        .style(Style::default().fg(*block_highlight_color));
    f.render_widget(response_block, area);

    let response_layout = Layout::default()
        .direction(Direction::Vertical)
        .constraints([Constraint::Percentage(10), Constraint::Percentage(90)].as_ref())
        .margin(1)
        .split(area);

    if let Some(response_data) = response {
        let status_code = response_data.status.to_string();
        let response_body = response_data.body.clone();
        let body_line_count = response_data.body_line_count.clone();

        let status_code_paragraph = Paragraph::new(status_code)
            .style(Style::default().fg(Color::Gray))
            .alignment(Alignment::Center);

        let formatted_json = match serde_json::from_str::<serde_json::Value>(&response_body) {
            Ok(json) => serde_json::to_string_pretty(&json).unwrap_or(response_body),
            Err(_) => response_body,
        };

        let response_body_paragraph = Paragraph::new(formatted_json)
            // .wrap(Wrap { trim: true })
            .style(Style::default().fg(Color::White))
            .block(Block::default().padding(Padding::horizontal(1)))
            .scroll((*res_scroll, 0));

        let scrollbar = Scrollbar::new(ScrollbarOrientation::VerticalRight)
            .begin_symbol(Some("↑"))
            .end_symbol(Some("↓"));

        let mut scrollbar_state =
            ScrollbarState::new(body_line_count as usize).position(*res_scroll as usize);

        f.render_widget(status_code_paragraph, response_layout[0]);
        f.render_widget(response_body_paragraph, response_layout[1]);
        f.render_stateful_widget(
            scrollbar,
            area.inner(&Margin {
                // using an inner vertical margin of 1 unit makes the scrollbar inside the block
                vertical: 1,
                horizontal: 0,
            }),
            &mut scrollbar_state,
        );
    }
}

fn render_footer(f: &mut Frame, mode: &Mode, area: Rect) {
    let footer_text = format!(
        "KEYBINDINGS: [Enter] Send Request | [i] Insert Mode | [q] Quit | MODE: {}",
        match *mode {
            Mode::Normal => "NORMAL",
            Mode::Insert => "INSERT",
        }
    );
    let footer = Paragraph::new(footer_text).style(Style::default().fg(Color::Gray));
    f.render_widget(footer, area);
}
