use reqwest::Method;

#[derive(Debug, PartialEq)]
pub enum Mode {
    Normal,
    Insert,
}

#[derive(Clone, Copy, Debug)]
pub enum HTTPMethod {
    GET,
    POST,
    PUT,
    PATCH,
    DELETE,
}

impl HTTPMethod {
    pub fn to_string(&self) -> &'static str {
        match *self {
            HTTPMethod::GET => "GET",
            HTTPMethod::POST => "POST",
            HTTPMethod::PUT => "PUT",
            HTTPMethod::PATCH => "PATCH",
            HTTPMethod::DELETE => "DELETE",
        }
    }

    pub fn from_str(s: &str) -> Option<Self> {
        match s.to_uppercase().as_str() {
            "GET" => Some(HTTPMethod::GET),
            "POST" => Some(HTTPMethod::POST),
            "PUT" => Some(HTTPMethod::PUT),
            "PATCH" => Some(HTTPMethod::PATCH),
            "DELETE" => Some(HTTPMethod::DELETE),
            _ => None,
        }
    }

    pub fn to_reqwest_method(&self) -> Method {
        match *self {
            HTTPMethod::GET => Method::GET,
            HTTPMethod::POST => Method::POST,
            HTTPMethod::PUT => Method::PUT,
            HTTPMethod::PATCH => Method::PATCH,
            HTTPMethod::DELETE => Method::DELETE,
        }
    }

    pub fn variants() -> Vec<Self> {
        vec![
            HTTPMethod::GET,
            HTTPMethod::POST,
            HTTPMethod::PUT,
            HTTPMethod::PATCH,
            HTTPMethod::DELETE,
        ]
    }
}
