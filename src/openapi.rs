use crate::app::{Api, ApiEndpoint};
use ratatui::prelude::*;
use serde_json;
use std::collections::HashMap;
use std::fs;
use tui_tree_widget::TreeItem;

/*
Convert OpenAPI JSON to TreeItems

Converts collection menu that looks like this -
> Tag
>> Path
>>> Summary

To menu that looks like this -
> Tag
>> Path
>>> <Http Method> Summary
*/
fn create_tree_item_with_method_summary<'a>(
    http_method: String,
    summary: String,
) -> TreeItem<'a, String> {
    let method_style = match http_method.to_uppercase().as_str() {
        "GET" => Style::default()
            .fg(Color::Green)
            .add_modifier(Modifier::BOLD),
        "POST" => Style::default()
            .fg(Color::Blue)
            .add_modifier(Modifier::BOLD),
        "PUT" => Style::default()
            .fg(Color::Yellow)
            .add_modifier(Modifier::BOLD),
        "DELETE" => Style::default().fg(Color::Red).add_modifier(Modifier::BOLD),
        "PATCH" => Style::default()
            .fg(Color::Magenta)
            .add_modifier(Modifier::BOLD),
        _ => Style::default()
            .fg(Color::White)
            .add_modifier(Modifier::BOLD),
    };

    let summary_style = Style::default().fg(Color::White);

    let method_text = format!("{}  ", http_method.to_uppercase().as_str());
    let summary_text = summary.clone();

    let line = Line::from(vec![
        Span::styled(method_text, method_style),
        Span::styled(summary_text, summary_style),
    ]);

    let text = Text::from(line);

    TreeItem::new_leaf(http_method, text)
}

pub fn build_api_map_from_openapi_json(
    json: serde_json::Value,
) -> Result<HashMap<String, HashMap<String, HashMap<String, ApiEndpoint>>>, std::io::Error> {
    // Define type aliases within the function
    type MethodDetails = HashMap<String, ApiEndpoint>;
    type PathDetails = HashMap<String, MethodDetails>;
    type TagMap = HashMap<String, PathDetails>;

    let mut tag_map: TagMap = HashMap::new();

    if let Some(paths) = json.get("paths") {
        for (path, methods) in paths.as_object().unwrap().iter() {
            for (method, details) in methods.as_object().unwrap().iter() {
                let api_map = ApiEndpoint {
                    http_method: method.clone(),
                    summary: details
                        .get("summary")
                        .map(|v| v.as_str().unwrap().to_string()),
                    parameters: details.get("parameters").cloned(),
                    responses: details.get("responses").cloned(),
                };

                if let Some(tags) = details.get("tags") {
                    for tag in tags.as_array().unwrap().iter() {
                        let tag_name = tag.as_str().unwrap().to_string();
                        tag_map
                            .entry(tag_name.clone())
                            .or_insert_with(HashMap::new)
                            .entry(path.clone())
                            .or_insert_with(HashMap::new)
                            .insert(method.clone(), api_map.clone());
                    }
                }
            }
        }
    }

    Ok(tag_map)
}

pub fn convert_hashmap_to_treeitems<'a>(
    hashmap: HashMap<String, HashMap<String, HashMap<String, ApiEndpoint>>>,
) -> Vec<TreeItem<'a, String>> {
    let mut tree_items = Vec::new();

    for (tag, paths) in hashmap {
        let mut path_items = Vec::new();

        for (path, methods) in paths {
            let mut method_items = Vec::new();

            for (method, details) in methods {
                let summary = details.summary.unwrap_or_else(|| "No summary".to_string());
                method_items.push(create_tree_item_with_method_summary(
                    method.clone(),
                    summary,
                ));
            }
            path_items.push(
                TreeItem::new(path.clone(), path, method_items).expect("Unique method items"),
            );
        }
        tree_items.push(TreeItem::new(tag.clone(), tag, path_items).expect("Unique path items"));
    }

    tree_items
}

pub fn populate_api_details(file_path: &str) -> Result<Api, std::io::Error> {
    // Read the OpenAPI JSON file
    let file_content = fs::read_to_string(file_path)?;
    let openapi_json: serde_json::Value = serde_json::from_str(&file_content)?;

    let api = Api {
        base_url: openapi_json
            .get("servers")
            .and_then(|s| s.get(0))
            .and_then(|s| s.get("url"))
            .and_then(|s| s.as_str())
            .unwrap_or("")
            .to_string(),
        api_map: build_api_map_from_openapi_json(openapi_json)?,
    };

    Ok(api)
}
