mod draw;
mod input;
use crate::app::App;
use draw::draw_ui;
use input::handle_input;

use std::{io, error};
use ratatui::{
    backend::CrosstermBackend,
    Terminal,
};

pub async fn event_loop<'a>(app: &mut App<'a>, terminal: &mut Terminal<CrosstermBackend<io::Stdout>>) -> Result<(), Box<dyn error::Error>> {
    loop {
        draw_ui(terminal, app)?;
        if let Err(e) = handle_input(app).await {
            eprintln!("Error handling input: {:?}", e);
            if e.kind() == io::ErrorKind::Other {
                break;
            }
        }
    }
    Ok(())
}
